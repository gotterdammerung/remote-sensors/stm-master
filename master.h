/* See LICENSE file for copyright and license details. */

#ifndef MASTER__HH
#define MASTER__HH

#include "antenna.h"
#include "encrypt.h"

#include "config.h"

class Master
{
public:
	Master();
	virtual ~Master();

	int getdata();
private:
	Antenna *antenna;
	Encrypt *encrypt;

	unsigned char output[TRANSFER_DATA];
	unsigned char input[TRANSFER_DATA];

	void copy(unsigned char *des, char *src);
};

#endif
