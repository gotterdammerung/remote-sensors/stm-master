/* See LICENSE file for copyright and license details. */

#include "master.h"

Master::Master()
{
	antenna = new Antenna();
	encrypt = new Encrypt((unsigned char*)aeskey, (unsigned char*)aesiv);
}

Master::~Master()
{
}

int
Master::getdata()
{
	char *text;

	antenna->enable();

	/* Send request */
	#ifdef DEBUG
	printf("[master] request data\n");
	#endif
	memset(&input[0], 0, sizeof(input));
	memset(&output[0], 0, sizeof(output));

	copy(input, (char*)"Request data");
	encrypt->encrypt(input, output);
	antenna->send((char*)output, TRANSFER_DATA);

	/* Wait response to accept */
	#ifdef DEBUG
	printf("[master] wait response to accept transfer data\n");
	#endif
	memset(&output[0], 0, sizeof(output));
	memset(&input[0], 0, sizeof(input));

	if (antenna->receive((char*)input, TRANSFER_DATA))
	{
		antenna->disable();
		return -1;
	}
	encrypt->decrypt(input, output);
	if (strcmp((char*)output, "Accept request data"))
	{
		antenna->disable();
		return -1;
	}

	/* Send wait data */
	#ifdef DEBUG
	printf("[master] send wait data\n");
	#endif
	memset(&input[0], 0, sizeof(input));
	memset(&output[0], 0, sizeof(output));

	copy(input, (char*)"Wait data");
	encrypt->encrypt(input, output);
	antenna->send((char*)output, TRANSFER_DATA);

	/* Wait data */
	#ifdef DEBUG
	printf("[master] wait data\n");
	#endif
	memset(&output[0], 0, sizeof(output));
	memset(&input[0], 0, sizeof(input));

	if (antenna->receive((char*)input, TRANSFER_DATA))
	{
		antenna->disable();
		return -1;
	}
	encrypt->decrypt(input, output);
	#ifdef DEBUG
	printf("[master] data: ");
	#endif
	printf("%s\n", output);

	/* Send Receive data */
	#ifdef DEBUG
	printf("[master] receive data\n");
	#endif
	memset(&input[0], 0, sizeof(input));
	memset(&output[0], 0, sizeof(output));

	copy(input, (char*)"Receive data");
	encrypt->encrypt(input, output);
	antenna->send((char*)output, TRANSFER_DATA);

	/* Wait response to accept */
	#ifdef DEBUG
	printf("[master] wait end request data\n");
	#endif
	memset(&output[0], 0, sizeof(output));
	memset(&input[0], 0, sizeof(input));

	if (antenna->receive((char*)input, TRANSFER_DATA))
	{
		antenna->disable();
		return -1;
	}
	encrypt->decrypt(input, output);
	if (strcmp((char*)output, "Close request data"))
	{
		antenna->disable();
		return -1;
	}

	antenna->disable();
	return 0;
}

void
Master::copy(unsigned char *des, char *src)
{
	int i;

	for (i=0; i<strlen((const char*)src); i++)
		des[i] = src[i];
}
