/* See LICENSE file for copyright and license details. */

#ifndef ENCRYPT__HH
#define ENCRYPT__HH

#include "mbed.h"
#include "mbedtls/aes.h"

#include "config.h"

class Encrypt
{
public:
	Encrypt(unsigned char *tkey, unsigned char *tiv);
	virtual ~Encrypt();

	int decrypt(unsigned char *cipher, unsigned char *text);
	int encrypt(unsigned char *text, unsigned char *cipher);

private:
	unsigned char key[AES_DATA];
	unsigned char iv[AES_IV];

	void copyiv(unsigned char *tiv);
	void loadiv(unsigned char *tiv);
	void loadkey(unsigned char *tkey);
};

#endif
