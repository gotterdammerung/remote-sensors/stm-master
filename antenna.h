/* See LICENSE file for copyright and license details. */

#ifndef ANTENNA__HH
#define ANTENNA__HH

#include "nRF24L01P.h"

#include "config.h"

class Antenna
{
public:
	Antenna();
	virtual ~Antenna();

	int enable();
	int disable();

	int send(char *data, int size);
	int receive(char *data, int size);
	int waitreceive(char *data, int size);

private:
	nRF24L01P *nrf;
	Timer *timer;

	int init();
};

#endif
