/* See LICENSE file for copyright and license details. */

#include "antenna.h"

Antenna::Antenna()
{
	/* mosi, miso, sck, csn, ce, irq */
	nrf = new nRF24L01P(
		nrf_mosi,
		nrf_miso,
		nrf_sck,
		nrf_csn,
		nrf_ce,
		nrf_irq
		);

	timer = new Timer();
	init();
}

Antenna::~Antenna()
{
}

int
Antenna::enable()
{
	#ifdef DEBUG
	printf("[antenna] enable\n");
	#endif
	nrf->enable();
	return 0;
}

int
Antenna::disable()
{
	#ifdef DEBUG
	printf("[antenna] disable\n");
	#endif
	nrf->disable();
	return 0;
}


int
Antenna::send(char *data, int size)
{
	#ifdef DEBUG
	printf("[antenna] send\n");
	#endif

	nrf->setTransferSize(size);
	nrf->write(NRF24L01P_PIPE_P0, data, size);

	return 0;
}

int
Antenna::receive(char *data, int size)
{
	#ifdef DEBUG
	printf("[antenna] wait data for 1s\n");
	#endif

	nrf->setTransferSize(size);
	timer->reset();
	timer->start();
	auto t = timer->elapsed_time().count();

	while (true)
	{
		t = timer->elapsed_time().count();
		if (nrf->readable())
			break;
		if (1e6<t)
		{
			#ifdef DEBUG
			printf("[antenna] no response\n");
			#endif

			timer->stop();
			return -1;
		}
	}

	#ifdef DEBUG
	printf("[antenna] available data\n");
	#endif

	/* Receive data*/
	nrf->read(NRF24L01P_PIPE_P0, data, size);

	return 0;
}

int
Antenna::waitreceive(char *data, int size)
{
	#ifdef DEBUG
	printf("[antenna] wait data forever\n");
	#endif

	nrf->setTransferSize(size);
	while (!nrf->readable());

	/* Receive data*/
	nrf->read(NRF24L01P_PIPE_P0, data, size);

	return 0;
}

int
Antenna::init()
{
	/* Start Antena */
	#ifdef DEBUG
	printf("[antenna] nrf24l01+ start\n");
	#endif
	nrf->powerUp();
	nrf->setTransferSize(1);
	nrf->setReceiveMode();
	nrf->enable();

	#ifdef DEBUG
	printf(	"[antenna] nrf24l01+ frequency    : %d MHz\n"
		"[antenna] nrf24l01+ output power : %d dBm\n"
		"[antenna] nrf24l01+ data rate    : %d kbps\n"
		"[antenna] nrf24l01+ TX address   : 0x%010llX\r\n"
		"[antenna] nrf24l01+ RX address   : 0x%010llX\r\n",
		nrf->getRfFrequency(),
		nrf->getRfOutputPower(),
		nrf->getAirDataRate(),
		nrf->getTxAddress(),
		nrf->getRxAddress()
		);
	#endif

	nrf->disable();

	return 0;
}
