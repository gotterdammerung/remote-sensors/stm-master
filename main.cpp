/* See LICENSE file for copyright and license details. */

#include "mbed.h"

#include "master.h"

#include "config.h"

/* Functions declarations */
void getsample(void);
void postevents(void);

/* Declarations */
Master *master;

/* Protocols */
BufferedSerial	pc(USBTX, USBRX);

/* Events */
EventQueue queue;
Event<void(void)> eventread(&queue, getsample);

/* Functions implementations */
void
getsample()
{
	int status;
	#ifdef DEBUG
	printf("[main] event start\n");
	#endif

	status = master->getdata();

	#ifdef DEBUG
	if (status)
		printf("[main] error\n");
	printf("[main] event end\n");
	#endif
	return;
}

void
postevents()
{
	eventread.post();
}

int
main()
{
	/* Protocols */
	pc.set_baud(serialbaudrate);

	/* Check FPU */
	#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
	#ifdef DEBUG
	printf("[main] Use FPU function (compiler enables FPU)\n");
	#endif
	#endif

	/* Init */
	master = new Master();

	/* Config events */
	Thread eventthread;

	//eventread.delay(0);
	eventread.period(TSAM);

	eventthread.start(callback(postevents));

	/* Initialization */
	queue.dispatch_forever();

	/* End */
	return 0;
}
