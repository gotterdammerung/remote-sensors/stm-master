/* See LICENSE file for copyright and license details. */

#ifndef CONFIG_HH
#define CONFIG_HH

#include "mbed.h"

#define DEBUG

/* Serial port */
static const unsigned int serialbaudrate = 115200;

/* Sample time*/
#define TSAM	5s
static const float tsample = 5;

/* NRF24L01P */
#define TRANSFER_DATA 32
static const PinName nrf_mosi	= D11;
static const PinName nrf_miso	= D12;
static const PinName nrf_sck	= D13;
static const PinName nrf_csn	= D10;
static const PinName nrf_ce	= D9;
static const PinName nrf_irq	= D7;

/* AES256 */
#define AES_DATA 32
#define AES_IV 16
static const unsigned char aeskey[32] = "f0d5ae71a256b1036159b44e653db4b";
static const unsigned char aesiv[AES_IV] = "e47ff1f9c352080";

#endif
